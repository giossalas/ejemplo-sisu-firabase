import { Button } from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import { useState } from 'react';
import firebase from './firebase';

const Formulario = ({setOpenForm,setReload}) => {

    const [name, setName] = useState('');
    const [lastname, setLastname] = useState('');

    const ref=firebase.firestore().collection('usuarios');
    const addUser = () => {
        console.log('Click');
        if(name.trim()==="" || lastname.trim()===""){
            return;
        }
        ref.add({
            nombre:name,
            apellido:lastname
        })
        .then(response=>{
            setName('');
            setLastname('');
            setReload(true);
            setOpenForm(false);
        })
        .catch(error=>{
            console.log('Error',error);
        })
    };

    return(
        <div className="wrapper">
            <div className="p-col-11 p-md-8 bg-white">
                <div className="p-fluid">
                    <div className="p-field">
                        <label>Nombre</label>
                        <InputText value={name} onChange={e=>setName(e.target.value)} />
                    </div>
                    <InputText value={lastname} onChange={e=>setLastname(e.target.value)} />
                </div>
                <div className="p-col-12 p-d-flex p-jc-end">
                        <Button type="button" onClick={e=>setOpenForm(false)} className="p-mr-4 p-button-secondary">Cancelar</Button>
                        <Button type="button" onClick={addUser}>Guardar</Button>
                </div>
            </div>
        </div>
    );

};

export default Formulario;