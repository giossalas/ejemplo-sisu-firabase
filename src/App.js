
import './App.css';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import Formulario from './Formulario';
import { useEffect, useState } from 'react';
import firebase from './firebase';

function App() {

    const [reload, setReload] = useState(true);
    const [users, setUsers] = useState([]);
    const [openForm, setOpenForm] = useState(false);

    const columns = [
        {field: 'nombre', header: 'Nombre'},
        {field: 'apellido', header: 'Apellido'}
    ];
    const dynamicColumns = columns.map((col,i) => {
        return <Column key={i} field={col.field} header={col.header} />;
    });

    /* --- Firabase --- */
    const ref=firebase.firestore().collection('usuarios');
    const getUsers = () => {
        ref.onSnapshot((querySnapshot) => {
            const usrs=[];
            querySnapshot.forEach( element => {
                usrs.push(element.data());
            });
            console.log(usrs);
            setUsers(usrs);
            setReload(false);
        });
    }

    useEffect(()=>{
        if(reload){
            getUsers();
        }
    },[reload]);

  return (
    <>
    {!openForm ?
        <div className="wrapper">
            <div className="p-col-11 p-md-8">
                <div className="p-d-flex p-jc-end p-mb-2">
                    <Button label="Añadir nuevo" onClick={e=>setOpenForm(true)} />
                </div>
                <DataTable value={users} className="p-shadow-1">
                    {dynamicColumns}
                </DataTable>
            </div>
        </div>
        :
        <Formulario setOpenForm={setOpenForm} setReload={setReload} />
    }
    </>
  );
}

export default App;
