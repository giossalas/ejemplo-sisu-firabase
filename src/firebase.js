import firebase from "firebase/app";
import "firebase/firestore";

const firestoreConfig={
    apiKey: "AIzaSyA5vyJUrtKS5JB4yPSZ2DvVafuhWE2bTAM",
    authDomain: "sisu-pp.firebaseapp.com",
    databaseURL: "https://sisu-pp-default-rtdb.firebaseio.com",
    projectId: "sisu-pp",
    storageBucket: "sisu-pp.appspot.com",
    messagingSenderId: "838981074540",
    appId: "1:838981074540:web:929184a8eb8f825abb6249"
};

firebase.initializeApp(firestoreConfig);

export default firebase;